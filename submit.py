# -*- coding: utf-8 -*-
import requests
from task import Task
from credentials import TOKEN, SERVER

data = {'url':Task().url}
with open('task.py','r') as f:
    data['task'] = f.read()

response = requests.post('%s/scraper/checkpoint_task/' % SERVER, headers={'Authorization':TOKEN}, json=data)
msg = response.json()['detail']
print(msg)

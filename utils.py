# -*- coding: utf-8 -*-
import re

def floatit(string):
    try:
        string = re.sub(r'[^ \d,\.]','',string)
        if re.search(r' \d+\.',string):
            string=string.replace(" ","")
        elif re.search(r' \d+,',string):
            string=string.replace(" ","")
            string=string.replace(",",".")
        elif re.search(r',\d+\.',string):
            string=string.replace(",","")
        elif re.search(r'\.\d+,',string):
            string=string.replace(".","")
            string=string.replace(",",".")
        elif re.search(r',',string):
            string=string.replace(",",".")
        return float(string)
    except:
        return 0.0

headers = {
            "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:79.0) Gecko/20100101 Firefox/79.0",
            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
            "Accept-Language": "en-US,en;q=0.5",
            "Connection": "keep-alive",
            "Upgrade-Insecure-Requests": 1,
            "If-None-Match": "1597679444-0",
            "Cache-Control": "max-age=0"
}
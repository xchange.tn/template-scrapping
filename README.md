# Welcome Scrapping Template
1- To start having project, you can install **Git**  and run
```
git clone git@gitlab.com:xchange.tn/template-scrapping.git
```
**OR**
You can easily download the project
2- To be able to work with us, you need a valid token to set in the file **credentials.py** , this is an example:
```
TOKEN = Token 0821d6610f980e1c4272f55a3b25e002791aecdc
```
3- To install requirements
```
python install -r requirements.txt
```
4- You are only allowed to change the file **task.py** and will all the needed code to run successfully the test
5- To run the test
```
python test.py
```
6- To submit for checkpoint or to reserve an url that you are working on
```
python submit.py
```

   

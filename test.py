# -*- coding: utf-8 -*-
import os
import unittest
import requests
from task import *
from credentials import TOKEN, SERVER

class TestParser(unittest.TestCase):
    task = Task()
    items = Task().parse_url()

    def test_valid_task(self):
        response = requests.get('%s/scraper/check_valid_task/?url=%s' % (SERVER,self.task.url), headers={'Authorization':TOKEN})
        msg = response.json()['detail']
        assert response.status_code==200, msg

    def test_count_items(self):
        msg = "Return items is empty"
        assert len(self.items) > 5, msg

    def test_currency_base(self):
        msg = "'currency_base' doesn't match required format (example: TND)"
        for item in self.items:
            assert re.match(r'^[A-Z]{3}$',item['currency_base']), msg

    def test_currency(self):
        msg = "'currency' doesn't match required format (example: TND)"
        for item in self.items:
            assert re.match(r'^[A-Z]{3}$',item['currency']), msg

    def test_unity(self):
        msg = "'unity' doesn't match required format (example: 1,10,100,1000)"
        for item in self.items:
            assert re.match(r'^10{,3}$',item['unity']), msg

    def test_buy_format(self):
        msg = "'buy' doesn't match required format (example: 1.234)"
        for item in self.items:
            assert re.match(r'^\d+\.\d+$',str(item['buy'])), msg

    def test_buy_to_zero(self):
        msg = "'buy' has to be different to 0.0"
        for item in self.items:
            assert item['buy'] > 0.0 , msg

    def test_buy_is_float(self):
        msg = "'buy' has to be float"
        for item in self.items:
            self.assertIsInstance(item['buy'],float)

    def test_sell_is_float(self):
        msg = "'sell' has to be float"
        for item in self.items:
            self.assertIsInstance(item['sell'],float)

    def test_sell_to_zero(self):
        msg = "'sell' has to be different to 0.0"
        for item in self.items:
            assert item['sell'] > 0.0 , msg

    def test_sell_format(self):
        msg = "'sell' doesn't match required format (example: 1.234)"
        for item in self.items:
            assert re.match(r'^\d+\.\d+$',str(item['sell'])), msg

    def test_currencies_relations(self):
        msg = "'currency' is same as 'currency_base'"
        for item in self.items:
            self.assertNotEqual(item['currency'], item['currency_base'])

    def test_sell_greater_than_buy(self):
        msg = "'sell' isn't greater than 'buy'"
        for item in self.items:
            assert item['sell'] > item['buy']

if __name__ == '__main__':
    test = unittest.main(exit=False)
    if test.result.wasSuccessful():
        os.system('python submit.py')